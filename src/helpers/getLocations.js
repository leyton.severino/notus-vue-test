import axios from 'axios';
const getLocations = async () => {
  try {
    const response = await axios.get(`http://localhost:3004/locations`);
    return response.data;
  } catch (e) {
    this.errors.push(e);
  }
};
export default getLocations;
