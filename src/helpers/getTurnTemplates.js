import axios from 'axios';
const getTurnTemplates = async () => {
  try {
    const response = await axios.get(`http://localhost:3004/turnTemplates`);
    return response.data;
  } catch (e) {
    this.errors.push(e);
  }
};
export default getTurnTemplates;
