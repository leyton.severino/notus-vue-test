import axios from 'axios';
const getPositions = async () => {
  try {
    const response = await axios.get(`http://localhost:3004/positions`);
    return response.data;
  } catch (e) {
    this.errors.push(e);
  }
};
export default getPositions;
