import axios from 'axios';
const getContracts = async () => {
  try {
    const response = await axios.get(`http://localhost:3004/contracts`);
    return response.data;
  } catch (e) {
    this.errors.push(e);
  }
};
export default getContracts;
