import axios from 'axios';
const getUsers = async () => {
  try {
    const response = await axios.get(`http://localhost:3004/users`);
    return response.data;
  } catch (e) {
    this.errors.push(e);
  }
};
export default getUsers;
