import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Calendar from '../views/Calendar.vue';
import Locations from '../views/Locations.vue';
import Users from '../views/Users.vue';
import TurnTemplates from '../views/TurnTemplates.vue';
Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/calendario',
    name: 'Calendar',
    component: Calendar,
  },
  {
    path: '/sucursales',
    name: 'Locations',
    component: Locations,
  },
  {
    path: '/usuarios',
    name: 'Users',
    component: Users,
  },
  {
    path: '/turnos',
    name: 'TurnTemplates',
    component: TurnTemplates,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
